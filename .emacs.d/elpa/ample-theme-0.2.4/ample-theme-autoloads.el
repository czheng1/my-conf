;;; ample-theme-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads nil "ample-theme" "ample-theme.el" (21112 20486
;;;;;;  157970 653000))
;;; Generated autoloads from ample-theme.el

(when load-file-name (add-to-list 'custom-theme-load-path (file-name-as-directory (file-name-directory load-file-name))))

;;;***

;;;### (autoloads nil nil ("ample-theme-pkg.el") (21112 20486 188538
;;;;;;  924000))

;;;***

(provide 'ample-theme-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; ample-theme-autoloads.el ends here
